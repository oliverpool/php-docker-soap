FROM php:7.4-cli-bullseye

RUN apt-get update -qq && apt-get install -qq -y \
    libpng-dev \
    libjpeg-dev \
    libxml2-dev \
    libzip-dev \
    git \
    && docker-php-ext-configure gd --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd zip soap bcmath intl pdo pdo_mysql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN php --version

RUN composer --version
